package oauth

import (
	"bookstore_oauth-go/oauth/clients"
	"bookstore_utils-go/rest_errors"
	"fmt"
	"github.com/stretchr/testify/assert"
	"io/ioutil"
	"net/http"
	"strings"
	"testing"
)

var (
	getAccessTokenFunc func(url string) (*http.Response, rest_errors.RestErr)
)
type clientRequestMock struct{}

func (cr *clientRequestMock) Get(request string) (*http.Response, rest_errors.RestErr) {
	return getAccessTokenFunc(request)
}

func TestOauthConstants(t *testing.T) {
	assert.EqualValues(t,  "X-Public", headerXPublic)
	assert.EqualValues(t,  "X-Client-Id", headerXClientId)
	assert.EqualValues(t,  "X-Caller-Id", headerXCallerId)
	assert.EqualValues(t,  "access_token", paramAccessToken)
}

func TestIsPublicNilRequest(t *testing.T) {
	assert.True(t, IsPublic(nil))
}

func TestIsPublicNoError(t *testing.T) {
	request := http.Request{
		Header: make(http.Header),
	}
	assert.False(t, IsPublic(&request))

	request.Header.Add("X-Public", "true")
	assert.True(t, IsPublic(&request))

}

func TestGetCallerIdNilRequest(t *testing.T) {

}

func TestGetCallerIdInvalidFormat(t *testing.T) {

}

func TestGetCallerIdNoError(t *testing.T) {

}

func TestGetAccessTokenInvalidRestClientResponse(t *testing.T){
	getAccessTokenFunc = func(url string) (*http.Response, rest_errors.RestErr){
		return &http.Response{
			StatusCode: http.StatusInternalServerError,
			Body:       ioutil.NopCloser(strings.NewReader(`{"code": 400, "error": "invalid rest client response"}`)),
		}, nil
	}
	clients.ClientRequest = &clientRequestMock{}

	response, err := getAccessToken("ABC123")
	assert.Nil(t, response)
	assert.NotNil(t, err)
	fmt.Println("this is the error: ", err)
}

//To FIX
//func TestGetAccessTokenOK(t *testing.T){
//	getAccessTokenFunc = func(url string) (*http.Response, rest_errors.RestErr){
//		return &http.Response{
//			StatusCode: http.StatusOK,
//			Body:       ioutil.NopCloser(strings.NewReader(`{"code": 400, "error": "invalid rest client response"}`)),
//		}, nil
//	}
//	clients.ClientRequest = &clientRequestMock{}
//
//	response, err := getAccessToken("ABC123")
//	assert.NotNil(t, response)
//	assert.Nil(t, err)
//	fmt.Println("this is the response: ", response)
//}

//func TestIsPublicTrue(t *testing.T) {
//
//}

