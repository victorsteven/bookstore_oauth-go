package clients

import (
	"bookstore_utils-go/rest_errors"
	"net/http"
)

var (
	ClientRequest clientInterface = &clientRequest{}
)

type clientInterface interface {
	Get(url string) (*http.Response, rest_errors.RestErr)
}
type clientRequest struct {}

func (cr *clientRequest) Get(url string) (*http.Response, rest_errors.RestErr) {
	request, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		return nil, rest_errors.NewInternalServerError("Cannot unmarshal json", err)
	}
	client := http.Client{}
	sendValue, err := client.Do(request)
	if err != nil {
		return nil, rest_errors.NewInternalServerError("error sending request", err)
	}
	return sendValue, nil
}
