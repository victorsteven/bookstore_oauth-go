package oauth

import (
	"bookstore_oauth-go/oauth/clients"
	"bookstore_utils-go/rest_errors"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
)

const (
	headerXPublic = "X-Public"
	headerXClientId = "X-Client-Id"
	headerXCallerId = "X-Caller-Id"
	paramAccessToken = "access_token" //the parameter we are using for in the url
)

type accessToken struct {
	Id string `json:"id"`
	UserId int64 `json:"user_id"`
	ClientId int64 `json:"client_id"`
}


//type oauthInterface interface {
//
//}

func IsPublic(request *http.Request) bool {
	if request == nil {
		return true
	}
	return request.Header.Get(headerXPublic) == "true"
}

func GetCallerId(request *http.Request) int64 {
	if request == nil {
		return 0
	}
	callerId, err := strconv.ParseInt(request.Header.Get(headerXCallerId), 10, 64)
	if err != nil {
		return 0
	}
	return callerId
}

func GetClientId(request *http.Request) int64 {
	if request == nil {
		return 0
	}
	clientId, err := strconv.ParseInt(request.Header.Get(headerXClientId), 10, 64)
	if err != nil {
		return 0
	}
	return clientId
}

func AuthenticateRequest(request *http.Request) rest_errors.RestErr {
	if request == nil {
		return nil
	}
	cleanRequest(request)
	accessTokenId := strings.TrimSpace(request.URL.Query().Get(paramAccessToken))
	if accessTokenId == "" {
		return nil
	}
	at, err := getAccessToken(accessTokenId)
	if err != nil {
		//what the condition below means is, even if an access token is not present(or an incorrect token is provided), dont throw an error, just return the public details(with limited info), for valid token, return the full info.
		if err.Status() == http.StatusNotFound {
			return nil
		}
		return err
	}
	request.Header.Add(headerXClientId, fmt.Sprintf("%v", at.ClientId))
	request.Header.Add(headerXCallerId, fmt.Sprintf("%v", at.UserId))

	return nil
}

func cleanRequest(request *http.Request) {
	if request == nil {
		return
	}
	request.Header.Del(headerXClientId)
	request.Header.Del(headerXCallerId)
}

func getAccessToken(accessTokenId string) (*accessToken, rest_errors.RestErr) {
	url := fmt.Sprintf("http://localhost:9999/oauth/access_token/%s", accessTokenId)
	response, err  := clients.ClientRequest.Get(url)
	if err != nil {
		return nil, rest_errors.NewInternalServerError("invalid rest client response when trying to get access token", errors.New("network timeout"))
	}
	//if response != nil {
	//	return nil, errors.NewInternalServerError("invalid rest client response when trying to get access token, timeout")
	//}
	bytes, errUtil := ioutil.ReadAll(response.Body)
	if errUtil != nil {
		return nil, rest_errors.NewInternalServerError("error when un marshall response", errors.New("invalid response"))
	}
	if response.StatusCode > 299 {
		restErr, err := rest_errors.NewRestErrorFromBytes(bytes)
		if err != nil {
			return nil, rest_errors.NewInternalServerError("invalid error interface when trying to get access gun", err)
		}
		return nil, restErr
	}
	var at accessToken
	if err := json.Unmarshal(bytes, &at); err != nil {
		return nil, rest_errors.NewInternalServerError("error when trying to unmarshal token response", errors.New("error processing json"))
	}
	return &at, nil
}