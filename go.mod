module bookstore_oauth-go

go 1.13

require (
	bookstore_utils-go v0.0.0
	github.com/stretchr/testify v1.4.0
)

replace bookstore_utils-go v0.0.0 => ../bookstore_utils-go
